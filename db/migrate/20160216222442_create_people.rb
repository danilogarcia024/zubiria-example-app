class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :nombre
      t.string :apellidos
      t.string :email
      t.boolean :alumno

      t.timestamps null: false
    end
  end
end
